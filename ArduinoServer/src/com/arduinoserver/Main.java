package com.arduinoserver;

import java.io.IOException;

import com.arduinoserver.server.ArduinoServer;

public class Main {
	public static void main(String[] args) {
		ArduinoServer server = new ArduinoServer();

		try {
			server.connect();
		} catch (IOException e) {
			System.out.println("Unable to start server: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
