package com.arduinoserver.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.sf.json.JSONObject;

public class ArduinoServer {
	public static final String HOST = "192.168.20.102";
	public static final int PORT = 9100;
	private ServerSocket serverSocket;
	ArduinoListener listener = new ArduinoListener();
	final List<PrintWriter> sockets = new CopyOnWriteArrayList<PrintWriter>();

	boolean pin2Set = false;
	boolean pin3Set = false;
	boolean pin4Set = false;

	public ArduinoServer() {
	}
	
	private String handleMessage(String message) {
		JSONObject object= new JSONObject();
		
		object.put("test", true);

		if (message.equals("pin2:true")) {
			pin2Set = !pin2Set;
		}
		
		if (message.equals("pin3:true")) {
			pin3Set = !pin3Set;
		}
		
		if (message.equals("pin4:true")) {
			pin4Set = !pin4Set;
		}

		if (message.equals("confirm")) {
			int sum = 0;
			
			sum += pin2Set ? 1 : 0;
			sum += pin3Set ? 2 : 0;
			sum += pin4Set ? 4 : 0;

			object.put("num1", sum);
		}

		return object.toString();
	}
	
	public void connect() throws IOException {
		listener.initialize(new Listener() {
			@Override
			public void arduinoMessageReceived(String message) {
				String msg = handleMessage(message);
				
				if (msg.length() > 0) {
					System.out.println(msg);
					
			    	//Broadcast
			    	for (PrintWriter writer : sockets) {
			    		String testData = getDataForTestData(msg);
		    			writer.println(testData);	
			    	}
				}
			}
			
		});

		System.out.println("Starting ArduinoServer at: " + HOST + ":" + PORT);
	    serverSocket = new ServerSocket(PORT);
		System.out.println("Waiting for connection");

		
		while(true) {
		    final Socket clientSocket = serverSocket.accept();
		    
		    Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
				    PrintWriter out = null;
				    BufferedReader in = null;
					try {
						out = new PrintWriter(clientSocket.getOutputStream(), true);
					    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
						    
						System.out.println("Connection with client established: " + clientSocket.getLocalAddress().getHostName());
				
					    String inputLine = "";
					    
						out.println("{}");
					    
					    sockets.add(out);
					    
					    while ((inputLine = in.readLine()) != null) {
					    	System.out.println("Message from client: " + inputLine);
					    	
					    	//Broadcast
					    	for (PrintWriter writer : sockets) {
					    		String testData = getDataForTestData(inputLine);
					    		
					    		if (testData != null) {
					    			writer.println(testData);	
					    		}
					    	}
					    }
					    
				    	System.out.println("Connection closed!");
	
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
					    try {
						    out.close();
							in.close();
						    clientSocket.close();
						    sockets.remove(out);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			});
		    
		    t.start();
		}
	}
	
	private String getDataForTestData(String str) {
		JSONObject object = JSONObject.fromObject(str);
		
		if ((object != null) && (object.optBoolean("test"))) {
			return object.toString();
		}
		else {
			return null;
		}
	}
}
