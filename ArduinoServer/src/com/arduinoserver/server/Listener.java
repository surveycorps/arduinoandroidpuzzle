package com.arduinoserver.server;

public interface Listener {
	public void arduinoMessageReceived(String message);
}
